const url="https://jsonplaceholder.typicode.com/users";
let xhr = new XMLHttpRequest();
let res = "";
xhr.open("GET", url);
xhr.onload = () => {
    res = JSON.parse(xhr.response)
    let table = document.getElementById("data");
    let thead=table.createTHead(-1);
    let row =thead.insertRow(-1);

    var css = 'table tbody tr:hover{cursor:pointer}';
    var style = document.createElement('style');

    if (style.styleSheet) {
        style.styleSheet.cssText = css;
    }
    else {
        style.appendChild(document.createTextNode(css));
    }

    document.getElementsByTagName('head')[0].appendChild(style);

    let cell = row.insertCell(-1)
    cell.innerHTML = "NAME";

    cell = row.insertCell(-1)
    cell.innerHTML = "E-Mail";

    cell = row.insertCell(-1)
    cell.innerHTML = "Phone";

    cell = row.insertCell(-1)
    cell.innerHTML = "Website";

    cell = row.insertCell(-1)
    cell.innerHTML = "Company Name";

    table.border = "2"
    console.log(res)
    let tbody=table.createTBody(-1);
    res.forEach(ele => {
        let row = tbody.insertRow(-1);
        row.setAttribute("id", ele.id)

        row.addEventListener("click",_=>{
            window.location.href=`./posts.html?id=${row.id}`;
            console.log(row.id)
        })

        let cell = row.insertCell(-1)
        cell.innerHTML = ele.name;

        cell = row.insertCell(-1)
        cell.innerHTML = ele.email;

        cell = row.insertCell(-1)
        cell.innerHTML = ele.phone;

        cell = row.insertCell(-1)
        cell.innerHTML = ele.website;

        cell = row.insertCell(-1)
        cell.innerHTML = ele.company.name;
    })
}
xhr.send();