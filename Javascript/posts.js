let url = new URL(document.URL)
let id = url.search[4]
let posts = [];
let userName = "";
let userEmail = "";
let selectedPosts=[]
fetch(`https://jsonplaceholder.typicode.com/users/${id}`)
.then(d => d.json())
.then(d => {
    userName = d.name;
    userEmail = d.email
})
.then(p => {
    fetch(`https://jsonplaceholder.typicode.com/posts?userId=${id}`)
    .then(p => p.json())
    .then(p => posts = p)
    .then(p => {
        let content = ``;
        posts.forEach(ele => {
            fetch(`https://jsonplaceholder.typicode.com/comments/?postId=${ele.id}`)
            .then(a => a.json())
            .then(a => {
                ele.comments = a
            })
            .then(a => {
                let card = `
                    <label id=${ele.id} onclick=addPost(event,${ele.id})>
                        <input type="checkbox">
                        <div class="post">
                            <div class="user">
                                <div class="right">
                                    <div class="edit">
                                        <i class="far fa-edit fa-lg"></i>
                                    </div>
                                    <div class="delete">
                                        <i class="far fa-trash-alt fa-lg" onclick="deletePost(${ele.id})"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="content" >
                                <div class="title" title="Title">
                                    ${ele.title}
                                </div>
                                <div class="body" title="Body">
                                    ${ele.body}
                                </div>
                            </div>
                            <div class="comments" title="Comments">
                                <div class="headding">
                                    Comments
                                </div>
                                <div class="comment">
                                    <div class="name">${ele.comments[0].name}</div>
                                    <div class="mail">${ele.comments[0].email}</div>
                                    <div class="inner-comment">
                                        ${ele.comments[0].body}
                                    </div>
                                </div>
                                <div class="comment">
                                    <div class="name">${ele.comments[1].name}</div>
                                    <div class="mail">${ele.comments[1].email}</div>
                                    <div class="inner-comment">
                                        ${ele.comments[1].body}
                                    </div>
                                </div>
                                <div class="comment">
                                    <div class="name">${ele.comments[2].name}</div>
                                    <div class="mail">${ele.comments[2].email}</div>
                                    <div class="inner-comment">
                                        ${ele.comments[2].body}
                                    </div>
                                </div>

                            </div>
                        </div>
                    </label>
                `;
                content += card;
                document.getElementsByClassName("all_posts")[0].innerHTML = content
            })
        })
    })
})
deletePost = (id) => {
    console.log("id", id)
    if(confirm("Do you want to delete "+id+" post")){
        document.getElementById(id).remove();}
    let id1 = selectedPosts.indexOf(id);
    if (id1 !== -1) {
        selectedPosts.splice(id1, 1);
    }
}

deleteMultiple=()=>{
    if(selectedPosts.length==0){
        alert("No posts are selected to delete")
    }
    else if(confirm("Do you want to delete "+selectedPosts)){
        selectedPosts.forEach(id=>{
            document.getElementById(id).remove();
        })
        selectedPosts=[]
    }
}

addPost=(e, id)=>{
    if (e.target.tagName !== "DIV") return;
    let id1 = selectedPosts.indexOf(id);
    if (id1 === -1) {
        selectedPosts.push(id);
    }
    else {
        selectedPosts.splice(id1, 1);
    }
};